## state-monad.l - Picolisp implmentation of the state monad
## Copyright (C) 2017  Christopher Howard

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

(de st-return (A) (list (list 'St) (list 'list A 'St)))

(de st-get () '((St) (list St St)))

(de st-put (A) (list (list 'St) (list 'list NIL A)))

(de runState (M St) (M St))

(de evalState (M St) (car (runState M St)))

(de execState (M St) (cadr (runState M St)))

(de st-bind @
   (let Res (next)
      (while (args)
         (setq Res (st->>= Res (next))) ) ) )

(de st-seq @
   (let Res (next)
      (while (args)
         (setq Res (st->> Res (next))) ) ) )

(de st->>= (@Act1 @Fact2)
   (let (@Act1 (cons 'quote @Act1)
         @Fact2 (cons 'quote  @Fact2))
   (macro
      '((St)
        (let (Ism (runState @Act1 St)
              Iv (car Ism)
              Is (cadr Ism)
              Act2 (@Fact2 Iv) )
           (runState Act2 Is) ) ) ) ) )

(de st->> (Act1 @Act2)
   (let @Act2 (cons 'quote @Act2)
      (st->>= Act1 (macro '((_) @Act2))) ) )

(de modify (@Fn)
   (let @Fn (cons 'quote @Fn)
      (st->>= (st-get)
         (macro '((N) (st-put (@Fn N)))) ) ) )

(de comp ()
   (st->>= (st-get)
      '((N)
        (st->>
           (st-put (+ 2 N))
           (st-return (* 3 N)))) ) )

(de comp2 ()
   (st->> (st-put 7) (st-get)) )

(de comp3 ()
   (modify
      '((N) (* 4 N)) ) )

(de comp4 ()
   (st-bind
      (st-get)
      '((R) (st-put (* 2 R)))
      '((_) (st-get))
      '((R) (st-put (+ 3 R))) ) )

(de comp5 ()
   (st-seq
      (st-put 5)
      (st-put 2)
      (st-put 3) ) )

(de comp6 ()
   (st-bind
      (st-get)
      '((V)
        (prinl V)
        (st-put (+ V 1)) )
      '((_) (st-get))
      '((V)
        (prinl (* V 2))
        (st-return NIL) ) ) )


      
